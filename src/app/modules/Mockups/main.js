import React, { useState } from "react";
import { useSubheader } from "../../../_metronic/layout";
import { Conexion } from "./conexion";
import { Card, CardBody } from "../../../_metronic/_partials/controls";
import { Button, ButtonToolbar } from "react-bootstrap";
import { Variables } from "./variables";
import { Mapeo } from "./mapeo";
import {DefinicionModelo} from "./definicioModelo"

export const Mockups = () => {
    const [isOpenConexion, setisOpenConexion] = useState(false);
    const [isOpenVariables, setisOpenVariables] = useState(false);
    const [isOpenMapeo, setisOpenMapeo] = useState(false);
    const [isOpenDefinicionModelo, setisOpenDefinicionModelo] = useState(false);
    const suhbeader = useSubheader();
    suhbeader.setTitle("Mockups");

    return (
        <Card>
            <CardBody>
                <Mapeo
                    show={isOpenMapeo}
                    onHide={() => {
                        setisOpenMapeo(false);
                    }}
                />
                <DefinicionModelo
                show={isOpenDefinicionModelo}
                onHide={() => {
                    setisOpenDefinicionModelo(false);
                }}
                />
                <Conexion
                    show={isOpenConexion}
                    onHide={() => {
                        setisOpenConexion(false);
                    }}
                />
                <Variables
                    show={isOpenVariables}
                    onHide={() => {
                        setisOpenVariables(false);
                    }}
                />

                <ButtonToolbar>
                    <div className="mb-3">
                        <span className="pr-4">
                            <Button variant="secondary" onClick={() => setisOpenMapeo(true)}>Mapeo</Button>
                        </span>
                        <span className="pr-4">
                            <Button variant="secondary" onClick={() => setisOpenDefinicionModelo(true)}>Definición del modelo</Button>
                        </span>
                        <span className="pr-4">
                            <Button variant="secondary" onClick={() => setisOpenConexion(true)}>Conexion</Button>
                        </span>
                        <span className="pr-4">
                            <Button variant="secondary" onClick={() => setisOpenVariables(true)}>Variables</Button>
                        </span>
                    </div>
                </ButtonToolbar>
            </CardBody>
        </Card>
    );
};
