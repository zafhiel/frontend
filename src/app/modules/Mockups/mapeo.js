import React from "react";
import { Button, Modal, Col, Row, Table } from "react-bootstrap";
import { Field, Form, Formik } from "formik";
import { Input, Select } from "../../../_metronic/_partials/controls";
import * as Yup from "yup";

import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { renderTree } from "./tree";
import { dataMapeo } from "./data/dataTree";


const valoresIniciales = {
    tennant: "Tennan 1",
};

const Validaciones = Yup.object().shape({
    tennant: Yup.string()
        .min(3, "Minimum 3 symbols")
        .max(50, "Maximum 50 symbols")
        .required("Firstname is required"),
});


export class Mapeo extends React.Component {

    render() {
        return (
            <Formik
                enableReinitialize={true}
                initialValues={valoresIniciales}
                validationSchema={Validaciones}
            >
                <Modal
                    {...this.props}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">
                            Mapeo
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form className="form form-label-right">
                            <Row>
                                <Col>
                                    <div className="form-group row">
                                        <div className="col-lg-4">
                                            <Field
                                                name="tennant"
                                                component={Input}
                                                // placeholder="Tennant"
                                                label="Tennant"
                                            />
                                        </div>
                                    </div>
                                </Col>
                                <Col>
                                    <div className="form-group row">
                                        <div className="col-lg-4">
                                            <Field
                                                name="Conexion"
                                                component={Input}
                                                // placeholder="Tennant"
                                                label="Conexion"
                                            />
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            <div className="separator separator-dashed my-7"></div>
                            <Row>
                                <Col>
                                    <span><h2>Propiedades de objetos</h2></span>
                                    <div className="form-group row">
                                        <div className="col-lg-4">
                                            <Select name="tipo" label="Nombre Objeto">
                                                <option value="0">Objeto1</option>
                                                <option value="1">Objeto2</option>
                                            </Select>
                                        </div>

                                    </div>
                                    <span><h2>Esquema Fuente</h2></span>
                                    <Table striped bordered hover>
                                        <thead>
                                            <tr>

                                                <th>Colunma</th>
                                                <th>Tipo de dato</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>name</td>
                                                <td>String</td>
                                            </tr>
                                            <tr>
                                                <td>Birth date</td>
                                                <td>Date</td>
                                            </tr>
                                        </tbody>
                                    </Table>

                                </Col>
                                <Col>
                                    <TreeView

                                        defaultCollapseIcon={<ExpandMoreIcon />}
                                        defaultExpanded={['root']}
                                        defaultExpandIcon={<ChevronRightIcon />}
                                    >
                                        {renderTree(dataMapeo)}
                                    </TreeView>
                                </Col>
                            </Row>
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.props.onHide}>Guardar</Button>
                    </Modal.Footer>
                </Modal>
            
            </Formik>
        );
    }
}
