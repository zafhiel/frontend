import FolderIcon from '@material-ui/icons/Folder';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import ListAltIcon from '@material-ui/icons/ListAlt';

export const iconsData = {

    folder : FolderIcon,
    person: PersonOutlineIcon,
    listAltIcon: ListAltIcon

}