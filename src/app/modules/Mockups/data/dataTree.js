export const dataMapeo = {
    id: 'root',
    name: 'Objetos',
    icon: 'folder',
    children: [
        {
            id: '1',
            name: 'Cliente',
            icon: 'person',
            children: [
                {
                    id: '2',
                    name: 'BaseProperties (Object)'
                },
                {
                    id: '3',
                    name: 'Llave (String)'
                },
                {
                    id: '4',
                    name: 'Nombre (String)'
                },
                {
                    id: '5',
                    name: 'Genero (String)'
                },
                {
                    id: '6',
                    name: 'Fecha de nacimiento (Date)'
                },
                {
                    id: '8',
                    name: 'Direcciones (Colletion)',
                    icon: 'folder',
                    children: [
                        {
                            id: '9',
                            name: 'Dirreccion (String)'
                        },
                        {
                            id: '10',
                            name: 'Coordenada (Object)'
                        },
                        {
                            id: '11',
                            name: 'Pais Departamento (DomainTree)'
                        }
                    ]
                }
            ]
        }
    ]
};

