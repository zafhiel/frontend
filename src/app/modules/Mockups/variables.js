import React from "react";
import {Button, Modal, Tab, Tabs} from "react-bootstrap";
import {Field, Form, Formik} from "formik";
import {Input, Select} from "../../../_metronic/_partials/controls";
import * as Yup from "yup";

const valoresIniciales = {
    tennant: "Tennan 1",
};

const Validaciones = Yup.object().shape({
    tennant: Yup.string()
        .min(3, "Minimum 3 symbols")
        .max(50, "Maximum 50 symbols")
        .required("Firstname is required"),
});

export class Variables extends React.Component {
    render() {
        return (
            <Formik
                enableReinitialize={true}
                initialValues={valoresIniciales}
                validationSchema={Validaciones}
            >
                <Modal
                    {...this.props}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">
                            Conexion
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
                            <Tab eventKey="basico" title="Basico">
                                <div className="separator my-7"></div>
                                <Form className="form form-label-right">
                                    <div className="form-group row">
                                        <div className="col-lg-4">
                                            <Field
                                                name="nombre"
                                                component={Input}
                                                // placeholder="Tennant"
                                                label="Nombre"
                                            />
                                        </div>
                                        <div className="col-lg-4">
                                            <Field
                                                name="valor"
                                                component={Input}
                                                label="Valor"
                                            />
                                        </div>
                                        <div className="col-lg-4">
                                            <Select name="tipoDato" label="Tipo Dato">
                                                <option value="0">Tipo1</option>
                                                <option value="1">Tipo2</option>
                                            </Select>
                                        </div>
                                    </div>
                                </Form>
                            </Tab>
                            <Tab eventKey="arhivo" title="Archivo">
                                <div className="separator my-7"></div>
                                <Form className="form form-label-right">
                                    <div className="form-group row">
                                        <div className="col-lg-4">
                                            <Field
                                                name="nombre"
                                                component={Input}
                                                label="Nombre"
                                            />
                                        </div>
                                    </div>
                                    {/* Email */}
                                    <h3>Propiedades</h3>
                                    <div className="form-group row">
                                        <div className="col-lg-4">
                                            <Select name="tipo" label="Tipo">
                                                <option value="0">Tipo1</option>
                                                <option value="1">Tipo2</option>
                                            </Select>
                                        </div>
                                        <div className="col-lg-4">
                                            <Field
                                                name="Nombre"
                                                component={Input}
                                                label="Nombre"
                                            />
                                        </div>
                                    </div>
                                    <h3>Conexion</h3>
                                    <div className="form-group row">
                                        {/* Gender */}
                                        <div className="col-lg-4">
                                            <Field
                                                name="usuario"
                                                component={Input}
                                                label="Usuario"
                                            />
                                        </div>
                                        <div className="col-lg-4">
                                            <Field
                                                name="puerto"
                                                component={Input}
                                                label="Puerto"
                                            />
                                        </div>
                                        <div className="col-lg-4">
                                            <Field
                                                name="clave"
                                                component={Input}
                                                label="Clave"
                                            />
                                        </div>
                                    </div>
                                </Form>
                            </Tab>
                            <Tab eventKey="contact" title="Contact" disabled></Tab>
                        </Tabs>



                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.props.onHide}>Close</Button>
                    </Modal.Footer>
                </Modal>

            </Formik>
        );
    }
}
