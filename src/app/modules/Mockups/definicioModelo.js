import React from "react";
import { Button, Modal, Col, Row, Table } from "react-bootstrap";
import { Field, Form, Formik } from "formik";
import { Input, Select } from "../../../_metronic/_partials/controls";
import * as Yup from "yup";

import { makeStyles } from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { renderTree } from "./tree";
import { dataMapeo } from "./data/dataTree";


const Validaciones = Yup.object().shape({
    tennant: Yup.string()
        .min(3, "Minimum 3 symbols")
        .max(50, "Maximum 50 symbols")
        .required("Firstname is required"),
});


export class DefinicionModelo extends React.Component {

    render() {
        return (
            <Formik
                enableReinitialize={true}
                validationSchema={Validaciones}
            >
                <Modal
                    {...this.props}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">
                            Definicion Modelo
                </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            <Col>
                                <TreeView
                                    defaultCollapseIcon={<ExpandMoreIcon />}
                                    defaultExpanded={['root']}
                                    defaultExpandIcon={<ChevronRightIcon />}
                                >
                                    {renderTree(dataMapeo)}
                                </TreeView>
                            </Col>
                            <Col>
                                
                                    <Form className="form form-label-right">
                                        <div className="form-group row">
                                            <div className="col-lg-4">
                                                <Select label="Negocio">
                                                    <option value="0">General</option>
                                                    <option value="1">Salud</option>
                                                    <option value="2">Caja</option>
                                                </Select>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <div className="col-lg-4">
                                                <Field
                                                    component={Input}
                                                    // placeholder="Tennant"
                                                    label="Nombre atributo"
                                                />
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <div className="col-lg-4">
                                                <Select label="Tipo atributo">
                                                    <option value="0">General</option>
                                                    <option value="1">Salud</option>
                                                    <option value="2">Caja</option>
                                                </Select>
                                            </div>
                                        </div>
                                    </Form>
                                
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.props.onHide}>Guardar</Button>
                    </Modal.Footer>
                </Modal>
            </Formik>

        );
    }
}
