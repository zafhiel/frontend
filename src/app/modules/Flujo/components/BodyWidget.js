"use strict";
const React = require("react");
const _ = require("lodash");
const TrayWidget_1 = require("./TrayWidget");
const TrayItemWidget_1 = require("./TrayItemWidget");
const react_diagrams_1 = require("@projectstorm/react-diagrams");
const react_canvas_core_1 = require("@projectstorm/react-canvas-core");
const styled_1 = require("@emotion/styled");
var S;
(function (S) {
    S.Body = styled_1.default.div `
		flex-grow: 1;
		display: flex;
		flex-direction: column;
		min-height: 100%;
	`;
    S.Header = styled_1.default.div `
		display: flex;
		background: #1E1E2D;
		flex-grow: 0;
		flex-shrink: 0;
		color: white;
		font-family: Helvetica, Arial, sans-serif;
		padding: 10px;
		align-items: center;
	`;
    S.Content = styled_1.default.div `
		display: flex;
		flex-grow: 1;
	`;
    S.Layer = styled_1.default.div `
		position: relative;
		flex-grow: 1;
	`;
})(S || (S = {}));
export class BodyWidget extends React.Component {
    render() {
        return (React.createElement(S.Body, null,
            React.createElement(S.Header, null,
                React.createElement("div", { className: "title" }, "Flujo de Tareas")),
            React.createElement(S.Content, null,
                React.createElement(TrayWidget_1.TrayWidget, null,
                    React.createElement(TrayItemWidget_1.TrayItemWidget, { model: { type: 'adminConection', name: "Oracle Caja", color: "rgb(192,255,0)" }, name: "Oracle Caja", color: "rgb(192,255,0)" }),
                    React.createElement(TrayItemWidget_1.TrayItemWidget, { model: { type: 'adminConection', name: "CSV Salud", color: "rgb(192,255,0)" }, name: "CSV Salud", color: "rgb(192,255,0)" }),

                    React.createElement(TrayItemWidget_1.TrayItemWidget, { model: { type: 'definedSources', name: "Financiero Ofuscado", color: "rgb(0,192,255)" }, name: "Financiero Ofuscado", color: "rgb(0,192,255)" }),

                    // Solo salida
                    React.createElement(TrayItemWidget_1.TrayItemWidget, { model: { type: 'column', name: "Telefono", color: "rgb(106,0,255)" }, name: "Telefono", color: "rgb(106,0,255)" }),
                    React.createElement(TrayItemWidget_1.TrayItemWidget, { model: { type: 'column', name: "Nombre Cliente", color: "rgb(106,0,255)" }, name: "Nombre Cliente", color: "rgb(106,0,255)" }),

                    React.createElement(TrayItemWidget_1.TrayItemWidget, { model: { type: 'taskMeta', name: "Separar Campos", color: "rgb(255,72,0)" }, name: "Separar Campos", color: "rgb(255,72,0)" }),
                    React.createElement(TrayItemWidget_1.TrayItemWidget, { model: { type: 'taskMeta', name: "Formato Fecha", color: "rgb(255,72,0)" }, name: "Formato Fecha", color: "rgb(255,72,0)" }),


                ),
                React.createElement(S.Layer, { onDrop: (event) => {
                            var data = JSON.parse(event.dataTransfer.getData('storm-diagram-node'));
                            var nodesCount = _.keys(this.props.app.getDiagramEngine().getModel().getNodes()).length;
                            var node = null;
                            console.log(data);
                            console.log(event);

                            node = new react_diagrams_1.DefaultNodeModel(data.name, data.color);
                            switch (data.type) {
                                case 'adminConection':
                                    node.addOutPort('Out');
                                    break;
                                case 'definedSources':
                                    node.addInPort('In');
                                    node.addOutPort('Out');
                                    break;
                                case 'column':
                                    node.addOutPort('Out');
                                    break;
                                case 'taskMeta':
                                    node.addInPort('In');
                                    break;
                            }

                            var point = this.props.app.getDiagramEngine().getRelativeMousePoint(event);
                            node.setPosition(point);
                            this.props.app.getDiagramEngine().getModel().addNode(node);
                            this.forceUpdate();
                        }, onDragOver: (event) => {
                            event.preventDefault();
                        } },
                    React.createElement(react_canvas_core_1.CanvasWidget, { className: "diagram-container", engine: this.props.app.getDiagramEngine() })))));
    }
}