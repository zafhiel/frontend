"use strict";
const React = require("react");
const styled_1 = require("@emotion/styled");
var S;
(function (S) {
    S.Tray = styled_1.default.div `
		min-width: 200px;
		background: #1E1E2D;
		flex-grow: 0;
		flex-shrink: 0;
	`;
})(S || (S = {}));
export class TrayWidget extends React.Component {
    render() {
        return React.createElement(S.Tray, null, this.props.children);
    }
}

//# sourceMappingURL=TrayWidget.js.map