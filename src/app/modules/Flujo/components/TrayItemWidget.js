"use strict";
const React = require("react");
const styled_1 = require("@emotion/styled");
var S;
(function (S) {
    S.Tray = styled_1.default.div `
		color: white;
		font-family: Helvetica, Arial;
		padding: 5px;
		margin: 0px 10px;
		border: solid 1px ${(p) => p.color};
		border-radius: 5px;
		margin-bottom: 2px;
		cursor: pointer;
	`;
})(S || (S = {}));
export class TrayItemWidget extends React.Component {
    render() {
        return (React.createElement(S.Tray, { color: this.props.color, draggable: true, onDragStart: (event) => {
                event.dataTransfer.setData('storm-diagram-node', JSON.stringify(this.props.model));
            }, className: "tray-item" }, this.props.name));
    }
}

//# sourceMappingURL=TrayItemWidget.js.map