import React from "react";
import { BodyWidget } from './components/BodyWidget';
import { Application } from './Application';

export const Flujo = () => {
    var app = new Application();

   return (
      <div className="content d-flex flex-column flex-column-fluid" id="kt_content">
          <style dangerouslySetInnerHTML={{__html: "\n        .diagram-container {\n          width: 100%; height: 100vh;\n        }\n      " }} />
          {/*<CanvasWidget className="diagram-container" engine={engine} />*/}
          <BodyWidget app={app} />
      </div>
  );
};