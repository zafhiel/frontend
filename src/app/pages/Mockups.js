import React, {useState} from "react";
import {useSubheader} from "../../_metronic/layout";
import {Conexion} from "../modules/Mockups/conexion";

import {Card, CardHeader, CardBody, CardHeaderToolbar, KTCodeExample} from "../../_metronic/_partials/controls";
import {Button, ButtonToolbar} from "react-bootstrap";

export const Mock1 = () => {
    const [isOpenConexion, setisOpenConexion] = useState(false);
    const suhbeader = useSubheader();
    suhbeader.setTitle("Mockups");

    return (
        <Card>
            <CardBody>
                <Conexion
                    show={isOpenConexion}
                    onHide={() => {
                        setisOpenConexion(false);
                    }}
                />
                <ButtonToolbar>
                    <div className="mb-3">
                  <span className="pr-4">
                    <Button variant="secondary" onClick={() => setisOpenConexion(true)}>Conexion</Button>
                  </span>
                  <span className="pr-4">
                    <Button variant="secondary" onClick={() => setisOpenConexion(true)}>Mapeo</Button>
                  </span>
                    </div>
                </ButtonToolbar>
            </CardBody>
        </Card>
    );
};
